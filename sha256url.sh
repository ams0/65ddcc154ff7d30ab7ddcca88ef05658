#!/bin/bash

#Usage: sha256url.sh <URL>

url=$1
curl -fLs $url | tee `basename $url`  | sha256sum && rm `basename $url`